<?php 
session_start();
  include "../../lib/koneksi.php";
  $session_admin = $_SESSION['admin']; 
  if(isset($_SESSION['admin']))
  {
    include "../template/header.php";
?>  
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="../dashboard">Home</a></li>
          <li class="breadcrumb-item"><a href="../peserta">Peserta</a></li>
          <li class="breadcrumb-item active">Baru</li>
        </ol>
        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                 
                </div>
              </div>
              <div class="col-md-8">
                <div class="card">
                  <div class="card-header">Tambah Data Calon Siswa</div>
                  <form action="insert_peserta_action.php" method="post">
                    <div class="card-body">
                      <div class="form-group">
                        <label for="company">Email</label>
                        <input class="form-control" id="txtEmail" type="email" placeholder="Email" name="email" value='admin@gmail.com'>
                      </div>
                      <div class="form-group">
                        <label for="company">Password</label>
                        <input class="form-control" id="txtPassword" type="Password" placeholder="Password" name="pass" value='admin'>
                      </div>
                      <div class="form-group">
                        <label for="company">No Ijazah</label>
                        <input class="form-control" id="txtIjazah" type="number" value="00000000" disabled placeholder="Nomor Ijazah" name="nisn">
                      </div>
                      <div class="form-group">
                        <label for="select1">Jurusan Pilihan</label>
                          <select class="form-control" id="select1" name="jurusan">
                            <?php 
                              $tampiljurusan = mysqli_query($mysqli,"SELECT * from jurusan");
                              while($jurusan = mysqli_fetch_array($tampiljurusan))
                              {
                            ?>
                            <option value="<?php echo $jurusan['Id_Jurusan']; ?>"><?php echo $jurusan['Nama_Jurusan']; ?></option>
                            <?php 
                              }
                            ?>
                          </select>
                      </div>
                      <hr class="mt-0">
                      <div class="form-group">
                        <label for="company">Nama</label>
                        <input class="form-control" id="txtNamaLengkap" type="text" placeholder="Nama" name="nama">
                      </div>
                      <div class="form-group">
                        <label for="select1">Jenis Kelamin</label>
                          <select class="form-control" id="select1" name="kelamin">
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label for="company">Tanggal Lahir</label>
                        <input class="form-control" id="date-input" value="2001-12-12" type="date" name="date-input" placeholder="Tanggal Lahir">
                      </div>
                      <div class="form-group">
                        <label for="company">Alamat</label>
                        <input class="form-control" id="company" type="text" placeholder="Alamat" value="medan" name="alamat">
                      </div>
                      <hr class="mt-0">
                      <div class="form-group">
                        <label for="company">Tamatan</label>
                        <input class="form-control" id="company" type="text" placeholder="Asal Sekolah" value="SMP" disabled name="smp">
                      </div>
                      <div class="form-group" style="display: none;">
                        <label for="company">Nilai Ujian Nasional</label>
                        <input class="form-control" id="company" value='0' type="number" step="any" placeholder="Nilai Ujian Nasional" name="un">
                      </div>
                      <div class="row align-items-center mt-3">
                      <div class="col-sm-6">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Simpan</button>
                      </div>
                      <div class="col-sm-6">
                        <a class="btn btn-outline-info btn-lg btn-block" href="../peserta">Batal</a>
                      </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.col-->
            </div>
            <!-- /.row-->
          </div>
        </div>
      </main>
      <script>
        document.getElementById('txtNamaLengkap').focus();
      </script>
<?php
    include "../template/footer.php";
  }
  else
  {
    header("location: ../login/");
  } 
?>