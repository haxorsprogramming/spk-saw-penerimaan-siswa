<!DOCTYPE html>
<html lang="en">
<head>
<title>SPK Penerimaan Siswa/i Baru - Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Unicat project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../styles/bootstrap4/bootstrap.min.css">
<link href="../plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="../plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="../styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="../styles/responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
			
		<!-- Top Bar -->
		<div class="top_bar">
			<div class="top_bar_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
								<ul class="top_bar_contact_list ml-auto">
								
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>
	</header>

	<!-- Features -->

	<div class="features">
		<div class="container">
			<div class="row features_row justify-content-center">
				
				<!-- Features Item -->
				<div class="col-md-6">
		          <div class="card-group">
		            <div class="card p-4">
		              <div class="card-body">
		                <h1>Login</h1>
		                <p class="text-muted">Masukkan email &amp; Password saat pendaftaran</p>
		                <form action="login_action.php" method="POST">
		                  <div class="input-group mb-3">
		                    <div class="input-group-prepend">
		                      <span class="input-group-text">
		                        <i class="icon-user"></i>
		                      </span>
		                    </div>
		                    <input class="form-control" type="email" placeholder="Email" name="email">
		                  </div>
		                  <div class="input-group mb-4">
		                    <div class="input-group-prepend">
		                      <span class="input-group-text">
		                        <i class="icon-lock"></i>
		                      </span>
		                    </div>
		                    <input class="form-control" type="password" placeholder="Password" name="password">
		                  </div>
		                  <div class="row">
		                    <div class="col-6">
		                      <button type="submit" class="btn btn-primary px-4">Login</button>
		                    </div>
		                    <div class="col-6 text-right">
		                      <button class="btn btn-link px-0" type="button"><a href="../daftar">Belum Punya akun?</a></button>
		                    </div>
		                  </div>
		                </form>
		              </div>
		            </div>
		          </div>
		        </div>

			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer>
	<div style='text-align:center;'>2020 &copy; M. Agung Aditya</div> 
	</footer>
</div>

<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../styles/bootstrap4/popper.js"></script>
<script src="../styles/bootstrap4/bootstrap.min.js"></script>
<script src="../plugins/greensock/TweenMax.min.js"></script>
<script src="../plugins/greensock/TimelineMax.min.js"></script>
<script src="../plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="../plugins/greensock/animation.gsap.min.js"></script>
<script src="../plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="../plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="../plugins/easing/easing.js"></script>
<script src="../plugins/parallax-js-master/parallax.min.js"></script>
<script src="../js/custom.js"></script>
</body>
</html>